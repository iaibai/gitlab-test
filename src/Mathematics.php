<?php
namespace App;

/**
 * Class Mathematics
 * @package App
 */
class Mathematics
{
    /**
     * @param array $numbers
     * @return int
     */
    public function sum(array $numbers): int
    {
        // Utilize existing functionality.
        return array_sum($numbers);
    }
}
