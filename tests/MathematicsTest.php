<?php

class MathematicsTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     */
    public function sum_should_add_together_numbers()
    {
        $maths = new App\Mathematics();
        $this->assertEquals(10, $maths->sum([ 2, 3, 5 ]));
    }

    /**
     * @test
     */
    public function sum_should_return_0_for_empty_array()
    {
        $maths = new App\Mathematics();
        $this->assertEquals(0, $maths->sum([]));
    }
}
