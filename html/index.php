<?php
require_once '../vendor/autoload.php';

$numbers = [3, 4, 5];
$maths = new App\Mathematics();

?><html>
<body>
The result of adding together <?php echo implode(', ', $numbers) . ' is ' . $maths->sum($numbers); ?>
</body>
</html>